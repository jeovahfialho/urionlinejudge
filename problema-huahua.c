#include <stdio.h>
#include <string.h>

int main(void) {
	// your code goes here
	
	char risada[51], palin[51];
	int len_r, len_p, aux, control, flag_p;
	
	scanf("%s", &risada);
	
	len_r = strlen(risada);
	
	flag_p = 1;
	
	control = 0;
	
	for (aux = 0; aux < len_r; aux++){
	    if ((risada[aux] == 'a') || (risada[aux] == 'e') || (risada[aux] == 'i') || (risada[aux] == 'o') || (risada[aux] == 'u')){
	         palin[control++] = risada[aux];
	    }
	}
	
	
	for (aux = 0; aux < (control/2); aux++){
	    if (palin[aux] != palin[control - aux - 1]){
	        printf("N\n");
	        flag_p = 0;
	        break;
	    }
	}
	
	if (flag_p == 1)
	    printf("S\n");
	
	return 0;
}